import flask
from flask import jsonify

import os
from dotenv import load_dotenv
import pymongo
load_dotenv()
login = os.getenv("LOGIN")
password = os.getenv("PASSWORD")
ip = os.getenv("IP")

client = pymongo.MongoClient(f"mongodb+srv://{login}:{password}@{ip}")
db = client.test
col = db.col
# mydict = { "name": "John", "address": "Highway 37" }
# x = col.insert_one(mydict)



app = flask.Flask(__name__)
app.config["DEBUG"] = True




# @app.route('/', methods=['GET','POST'])
# def home():
#     d = [{"id": 4,"score":4},{"id": 3,"score":4}]
#     return jsonify(d)

def add_team(name,score):
    pass


@app.route('/get_all', methods=['GET','POST'])
def get_all():
    d = []
    for idx,record in enumerate(col.find()):
        del record['_id']
        record['id'] = idx
        d.append(record)
    return jsonify(d)

@app.route('/post_results', methods=['GET','POST'])
def post_results():
    d = []
    for idx,record in enumerate(col.find()):
        del record['_id']
        record['id'] = idx
        d.append(record)
    return jsonify(d)

app.run(host="0.0.0.0")
